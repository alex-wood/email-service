import unittest
import json
import email_service
from mock import Mock, patch

class EmailServiceTestCase(unittest.TestCase):
    
    def setUp(self):
        email_service.app.config['TESTING'] = True
        self.app = email_service.app.test_client()

    def test_normal_functionality(self):
        response = self.app.post(
                        '/email', 
                        data = json.dumps({
                                    "to": "alex.wood.86@gmail.com",
                                    "to_name": "Ms. Fake",
                                    "from": "no-reply@awesomehosting.com", 
                                    "from_name": "Awesome Hosting",
                                    "subject": "A Message from Awesome Hosting", 
                                    "body": "<h1>Your Bill</h1><p>$33</p>"
                                }),
                        content_type = 'application/json'
                    )
        
        assert response.status_code == 200
    
    def test_send_incomplete_payload(self):
        """ The provided payload is missing the 'from' parameter """

        response = self.app.post(
                        '/email', 
                        data = json.dumps({
                                    "to": "fake@example.com",
                                    "to_name": "Ms. Fake",
                                    "from": "no-reply@awesomehosting.com", 
                                    "subject": "A Message from Awesome Hosting", 
                                    "body": "<h1>Your Bill</h1><p>$33</p>"
                                }),
                        content_type = 'application/json'
                    )

        assert response.status_code == 400

    def test_invalid_parameter(self):
        """ Attempts to send an email to an invalid email address """

        response = self.app.post(
                        '/email', 
                        data = json.dumps({
                                    "to": "fakeexample.com",
                                    "to_name": "Ms. Fake",
                                    "from": "no-reply@awesomehosting.com", 
                                    "from_name": "Awesome Hosting",
                                    "subject": "A Message from Awesome Hosting", 
                                    "body": "<h1>Your Bill</h1><p>$33</p>"
                                }),
                        content_type = 'application/json'
                    )

        assert response.status_code == 400

    @patch('mailers.mailgun_mailer.MailGunMailer.send')
    def test_invoke_mailgun_failure(self, mock_mailgun_send):
        """ Test the service's failover capability by invoking a MailGun
            failure (the primary mailer)
        """

        mock_mailgun_send.return_value = Mock(ok=False, status_code=400)

        response = self.app.post(
                        '/email', 
                        data = json.dumps({
                                    "to": "alex.wood.86@gmail.com",
                                    "to_name": "Ms. Fake",
                                    "from": "no-reply@awesomehosting.com", 
                                    "from_name": "Awesome Hosting",
                                    "subject": "A Message from Awesome Hosting", 
                                    "body": "<h1>Your Bill</h1><p>$33</p>"
                                }),
                        content_type = 'application/json'
                    )

        assert response.status_code == 200
        assert b'Mandrill' in response.data

    @patch('mailers.mailgun_mailer.MailGunMailer.send')
    @patch('mailers.mandrill_mailer.MandrillMailer.send')
    def test_invoke_mailgun_failure(self, mock_mailgun_send, mock_mandrill_send):
        """ Invoke failures in both mailers to ensure the service fails
            elegantly.
        """

        mock_mailgun_send.return_value = Mock(ok = False, status_code = 400)
        mock_mandrill_send.return_value = Mock(ok = False, status_code = 400)

        response = self.app.post(
                        '/email', 
                        data = json.dumps({
                                    "to": "alex.wood.86@gmail.com",
                                    "to_name": "Ms. Fake",
                                    "from": "no-reply@awesomehosting.com", 
                                    "from_name": "Awesome Hosting",
                                    "subject": "A Message from Awesome Hosting", 
                                    "body": "<h1>Your Bill</h1><p>$33</p>"
                                }),
                        content_type = 'application/json'
                    )

        assert response.status_code == 400


if __name__ == '__main__':
    unittest.main()