from mailers.mailgun_mailer import MailGunMailer
from mailers.mandrill_mailer import MandrillMailer

class EmailDispatcher:
	""" Delegates email requests to the appropriate delivery service """

	SUCCESS_RESPONSE = "Email sent successfully with {0}"
	ERROR_RESPONSE = "Unable to send an email with either provider"

	def __init__(self):
		# MailGun serves as the primary email provider:
		self.mailer = MailGunMailer()

	def send(self, payload):
		""" Attempts to send an email through MailGun and switches to 
			Mandrill if it fails.
		"""

		response = self.mailer.send(payload)
		message = None

		if response.ok:
			message = self.SUCCESS_RESPONSE.format(self.mailer.name())
		else:
			self.mailer = MandrillMailer()

			response = self.mailer.send(payload)

			if response.ok:
				message = self.SUCCESS_RESPONSE.format(self.mailer.name())
			else:
				message = self.ERROR_RESPONSE

		return (response.status_code, message)