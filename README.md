## Email Sending Service

Provides an abstraction between two email delivery services (MailGun and Mandrill) in order to provide a simple interface for sending email messages via HTTP and the ability to handle delivery service failures (i.e., fail over capability). 

### Prerequisites
* Python version 2.7.6 or higher with the following packages:
	- flask
	- html2text
	- validate_email
	- pyyaml
* MailGun and Mandrill accounts

### Running the application
Add your MailGun and Mandrill API keys and related settings in the `config/mailers.yaml` configuration file before firing up the application.

Simply navigate to the application root and invoke the following command at the command line to launch the app:
`python email_service.py`

Once the app is running, emails can be sent by making POST requests to the '/email' endpoint. The payload **must** contain the following POST parameters: `to, to_name, from, from_name, subject` and `body`, all of which should contain string arguments. Here's an example payload:
```
{
	"to": "fake@example.com",
	"to_name": "Ms. Fake",
	"from": "noreply@awesomehosting.com", 
	"from_name": "Awesome Hosting",
	"subject": "A Message from Awesome Hosting", 
	"body": "<h1>Your Bill</h1><p>$33</p>"
}
```

A "body_text" post parameter can also be included, but it isn't required.