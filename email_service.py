import os
import json
import requests
import httplib
import yaml
import html2text
from sqlite3 import dbapi2 as sqlite3
from flask import Flask, request, session, g, url_for, \
     jsonify, make_response
from validate_email import validate_email
from utilities.email_dispatcher import EmailDispatcher

app = Flask(__name__)

with open('config/mailers.yaml') as config_file:
    app.config.update(yaml.safe_load(config_file))

REQUIRED_PARAMETERS = ['to', 'to_name', 'from', 'from_name', 'subject', 'body']

@app.route('/email', methods=['POST'])
def send_email():
    email_payload = request.json

    try:
        validate_post_parameters(email_payload)
    except Exception as ex:
        return make_response(
                    jsonify(
                        status_code = httplib.BAD_REQUEST, 
                        message = ex.message
                    ), 
                    httplib.BAD_REQUEST
                )

    # If a plain text body isn't provided, convert the HTML body to plain 
    # text and add it to the payload:
    if not 'body_text' in email_payload:
        email_payload['body_text'] = html2text.html2text(email_payload['body'])

    status_code, message = EmailDispatcher().send(email_payload)

    return make_response(
                jsonify(
                    status_code = status_code,
                    message = message
                ),
                status_code
            )

def validate_post_parameters(parameters):
    # Ensure the required parameters have been provided:
    for required_param in REQUIRED_PARAMETERS:
        if not required_param in parameters:
            raise Exception("{0} is a required parameter".format(required_param))

    # Validate the payload arguments:
    error_message = None
    if not validate_email(parameters.get('to')):
        error_message = "{0} is not a valid recipient address".format(parameters.get('to'))
    elif not validate_email(parameters.get('from')):
        error_message = "{0} is not a valid sender address".format(parameters.get('from'))

    if error_message:
        raise Exception(error_message)


if __name__ == '__main__':
    app.run()