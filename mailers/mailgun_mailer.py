import requests
from flask import current_app
from mailer import Mailer

class MailGunMailer(Mailer):
	def __init__(self):
		mailgun_config = current_app.config.get('mailgun')

		self.base_url = mailgun_config.get('base_url')
		self.domain = mailgun_config.get('domain')
		self.api_key = mailgun_config.get('api_key')

	def name(self):
		return "MailGun"

	def send(self, payload):
		return requests.post(
		    self.base_url + self.domain,
		    auth = ("api", self.api_key),
		    data = {
						"from": "{0} <{1}>".format(payload.get('from_name'), payload.get('from')),
		      			"to": "{0} <{1}>".format(payload.get('to_name'), payload.get('to')),
		          		"subject": payload.get('subject'),
		          		"html": payload.get('body'),
		          		"text": payload.get('body_text')
		      		}
          		)