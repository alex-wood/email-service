class Mailer:
	""" Serves as a general interface for email delivery
		services
	"""

	def name(self):
		raise NotImplementedError
		
	def send(self):
		raise NotImplementedError