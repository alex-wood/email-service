import requests
import json
from flask import current_app
from mailer import Mailer

class MandrillMailer(Mailer):
	def __init__(self):
		mandrill_config = current_app.config.get('mandrill')

		self.base_url = mandrill_config.get('base_url')
		self.send_endpoint = mandrill_config.get('send_endpoint')
		self.api_key = mandrill_config.get('api_key')

	def name(self):
		return "Mandrill"

	def send(self, payload):
		return requests.post(
	        self.base_url + self.send_endpoint,
	        data = json.dumps({
				    	"key": self.api_key,
					    "message": {
				      		"html": payload.get('body'),
					        "text": payload.get('body_text'),
			        		"subject": payload.get('subject'),
					        "from_email": payload.get('from'),
					        "from_name": payload.get('from_name'),
					        "to": [
					            {
					                "email": payload.get('to'),
					                "name": payload.get('to_name'),
					                "type": "to"
					            }
					        ]
			        	}
		        	}
		        )
        	)